# Notifybar

## Table of contents

- Introduction
- Installation
- Requirements
- Configuration
- Maintainers


## Introduction

Notifybar module provide you functionality to show the notification bar at the page top and bottom.
The block with name "Notifybar" to show on the site.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Requirements

This module requires the following modules:

- [Block](https://www.drupal.org/project/block)


## Configuration

1. Go to Home » Administration » Structure » Block layout» Add Notifybar Block
2. Add your message, button text, button link, background colour and text colour.
3. If you want to hide the notification bar from top then select the none option from "Show notifybar" bar drop-down.
4. You can use the block("Notifybar") to show in the specific region
5. Clear drupal caches in fully theme-able colored horizontal bars on top of the page.


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
