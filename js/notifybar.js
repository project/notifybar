(function ($, Drupal) {
  Drupal.behaviors.notifybar = {
    attach: function (context, settings) {

      var interval = drupalSettings.notifybar.interval;
      var period = drupalSettings.notifybar.period;

      var expiresValue = function getCookiesTime(interval,period) {
        var date = new Date();
        switch(period) {
          case 'Minutes':
            date.setTime(
              date.getTime() + interval * 60 * 1000
            );
            return date;
              break;
          case 'Hours':
            date.setTime(
              date.getTime() + interval * 60 * 60 * 1000
            );
            return date;
              break;
          case 'Days':
            date.setTime(
              date.getTime() + interval * 24 * 60 * 60 * 1000
            );
            return date;
              break;
          /*case 'Months':
            date.setMonth(date.getMonth() + interval);
            return date;
              break;
          case 'Years':
            date.setYear(date.getYear() + interval);
            return date;
              break;*/
        }
      }

      const iCookieLength = 2; // Cookie length in mnt
      const sCookieName = 'notify-bar';
      const sCookieContents = '1';

      function setCookie() {
      /*var date = new Date();
        date.setTime(
          date.getTime() + iCookieLength * 60 * 1000
        );*/
        var expireDate = expiresValue(interval,period);
        document.cookie = `${sCookieName}=${sCookieContents}; expires=${expireDate.toGMTString()}; path=/;`;
      }

      function getCookie() {
        const cookieArray = document.cookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
          let cookie = cookieArray[i];
          // Remove space between cookies.
          while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1);
          }
          // Cookie found? Return the cookie value.
          if (cookie.indexOf(sCookieName) === 0) {
            return cookie.substring(sCookieName.length, cookie.length);
          }
        }
        return null; // No cookie found.
      }

      const notifyBarCookie = getCookie();
      if (!notifyBarCookie) {
        $('.notify-bar-wrapper').show();
      }
      else {
        $('.notify-bar-wrapper').hide();
      }

      $('#notify-bar-btn',context).click(function(){
        setCookie();
        $('.notify-bar-wrapper').slideToggle('slow');
      });


    }
  }
})(jQuery, Drupal);


