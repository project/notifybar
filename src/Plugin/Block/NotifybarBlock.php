<?php
namespace Drupal\notifybar\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Notify Bar' block.
 *
 * @Block(
 *  id = "notifybar",
 *  admin_label = @Translation("Notifybar"),
 * )
*/

class NotifybarBlock extends BlockBase {

 /**
  * {@inheritdoc}
  */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['notifybar'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Notifybar Settings Form'),
    ];
    $form['notifybar']['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => isset($config['message']) ? $config['message'] : '',
      '#required' => TRUE,
    ];
    $form['notifybar']['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CTA Button'),
      '#default_value' => isset($config['button']) ? $config['button'] : '',
      '#required' => TRUE,
    ];
    $form['notifybar']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => array_combine(['top','bottom'], ['Top','Bottom']),
      '#default_value' => isset($config['position']) ? $config['position'] : '',
      '#empty_option' => '-None-',
    ];
    $form['notifybar']['background'] = [
      '#type' => 'color',
      '#title' => $this->t('Background'),
      '#default_value' => isset($config['background']) ? $config['background'] : '',
    ];
    $form['notifybar']['color'] = [
      '#type' => 'color',
      '#title' => 'Message Color',
      '#default_value' => isset($config['color']) ? $config['color'] : '',
    ];

    $form['notifybar']['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Cookie Interval',
      '#attributes' => ['class' => ['container-inline'],'style'=>'border: none; padding: 20px 0 0 0;'],
    ];
    $form['notifybar']['fieldset']['interval'] = [
       '#type' => 'number',
       '#default_value' => isset($config['interval']) ? $config['interval'] : '',
       '#required' => TRUE,
    ];
    $day = ['Minutes','Hours','Days'];
    $form['notifybar']['fieldset']['period'] = [
      '#type' => 'select',
      '#options' => array_combine($day,$day),
      '#default_value' => isset($config['period']) ? $config['period'] : '',
    ];

    return $form;
  }

 /**
  * {@inheritdoc}
  */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->setConfigurationValue('message', $form_state->getValue(['notifybar','message']));
    $this->setConfigurationValue('button', $form_state->getValue(['notifybar','button']));
    $this->setConfigurationValue('position', $form_state->getValue(['notifybar','position']));
    $this->setConfigurationValue('background', $form_state->getValue(['notifybar','background']));
    $this->setConfigurationValue('color', $form_state->getValue(['notifybar','color']));
    $this->setConfigurationValue('interval', $form_state->getValue(['notifybar','fieldset','interval']));
    $this->setConfigurationValue('period', $form_state->getValue(['notifybar','fieldset','period']));
  }

 /**
  * {@inheritdoc}
  */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'notifybar_template',
      '#message' => isset($config['message']) ? $config['message'] : '',
      '#button' => isset($config['button']) ? $config['button'] : '',
      '#position' => isset($config['position']) ? $config['position'] : '',
      '#background' => isset($config['background']) ? $config['background'] : '',
      '#color' => isset($config['color']) ? $config['color'] : '',
      '#interval' => isset($config['interval']) ? $config['interval'] : '',
      '#period' => isset($config['period']) ? $config['period'] : '',
    ];
  }

}
